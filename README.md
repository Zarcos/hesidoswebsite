# Hesidos website portal

**DEVEL** branch slouží k vývoji  
**STABLE** branch beží na serveru

## Main page - Gatsby - Frontend

Hlavní stránku pro prezentaci L2Hesidos pro veřejnost  
Zdrojový kod ve složce client/gatsby-web  
URL: [https://l2hesidos.com/](https://l2hesidos.com/)  
Design pomocí [https://emotion.sh](https://emotion.sh/docs/introduction)

## Account control panel - React - Frontend

Stránka pro tvorbu a obsluhu webového a herního učtu  
Zdrojový kod ve složce client/react-cp 
URL: [https://l2hesidos.com/](https://l2hesidos.com/)  
Komuniku s backend serverem pomocí Graphql  
Design pomocí [https://ant.design/](https://ant.design/)

## Backend server - NodeJS - Backend

Stará se o zpracování dat mezi oběma frontend aplikacemi a databazí/herním serverem.  
Přijímá informace o platbých ze serveru Paypal  
Zdrojový kod ve složce server  
URL: [https://l2hesidos.com/api](https://l2hesidos.com/api) - GraphQL API  
URL: [https://l2hesidos.com/ipn](https://l2hesidos.com/ipn) - REST komunikující s paypal API